output "host" {
  description = "The hostname (in form of URI) of the Kubernetes API."
  value = kind_cluster.cluster.endpoint
}

output "client_certificate" {
  description = "PEM-encoded client certificate for TLS authentication."
  value = kind_cluster.cluster.client_certificate
}

output "client_key" {
  description = "PEM-encoded client certificate key for TLS authentication."
  value = kind_cluster.cluster.client_key
  sensitive = true
}

output "cluster_ca_certificate" {
  description = "PEM-encoded root certificates bundle for TLS authentication."
  value = kind_cluster.cluster.cluster_ca_certificate
}
