# Retrieve the kapp-controller release manifests
data "http" "manifests" {
  url = "https://github.com/vmware-tanzu/carvel-kapp-controller/releases/download/${var.release}/release.yml"
}

# Split the Kubernetes manifests into multiple files so they can be treated as separate Terraform resources
data "kubectl_file_documents" "manifests" {
  content = data.http.manifests.body
}

locals {
  # Convert from YAML string into Terraform structures
  manifests = [for v in data.kubectl_file_documents.manifests.documents : {
    data : yamldecode(v)
    content : v
    }
  ]
}

# Apply each manifest as a resource
resource "kubectl_manifest" "manifests" {
  for_each   = { for v in local.manifests : lower(join("/", compact([v.data.apiVersion, v.data.kind, lookup(v.data.metadata, "namespace", ""), v.data.metadata.name]))) => v.content }
  yaml_body  = each.value
}
