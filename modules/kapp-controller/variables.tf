variable "release" {
  description = "The release version of kapp-controller to install."
  type = string
  default = "v0.20.0"
}
