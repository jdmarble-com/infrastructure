resource "kubernetes_service_account" "deploy-configuration" {
  metadata {
    name = "deploy-configuration"
    namespace = "kapp-controller-packaging-global"
  }
}

resource "kubernetes_role" "deploy-configuration" {
  metadata {
    name = "deploy-configuration"
    namespace = "kapp-controller-packaging-global"
  }

  rule {
    api_groups = [""]
    resources = ["configmaps"]
    verbs = ["get", "list", "create", "update", "delete"]
  }
}
resource "kubernetes_role_binding" "deploy-configuration" {
  metadata {
    name = "deploy-configuration"
    namespace = "kapp-controller-packaging-global"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "Role"
    name = kubernetes_role.deploy-configuration.metadata[0].name
  }
  subject {
    kind = "ServiceAccount"
    name = kubernetes_service_account.deploy-configuration.metadata[0].name
    namespace = "kapp-controller-packaging-global"
  }
}

resource "kubernetes_cluster_role" "deploy-configuration" {
  metadata {
    name = "deploy-configuration"
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs = ["*"]
  }
}
resource "kubernetes_cluster_role_binding" "deploy-configuration" {
  metadata {
    name = "deploy-configuration"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "ClusterRole"
    name = kubernetes_cluster_role.deploy-configuration.metadata[0].name
  }
  subject {
    kind = "ServiceAccount"
    name = kubernetes_service_account.deploy-configuration.metadata[0].name
    namespace = "kapp-controller-packaging-global"
  }
}

resource "kubectl_manifest" "configuration-app" {
  yaml_body = <<YAML
    apiVersion: kappctrl.k14s.io/v1alpha1
    kind: App
    metadata:
      name: configuration
      namespace: kapp-controller-packaging-global
    spec:
      serviceAccountName: ${kubernetes_service_account.deploy-configuration.metadata[0].name}
      fetch:
        - imgpkgBundle:
            image: registry.gitlab.com/jdmarble-com/configuration:latest

      template:
        - sops:
            pgp:
              privateKeysSecretRef:
                name: pgp-secrets
            paths:
              - "config/secrets"
        - ytt:
            paths:
              - "config"
      deploy:
        - kapp: {}
  YAML
}