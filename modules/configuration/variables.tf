variable "base" {
  description = "The base DNS name to use for service URLs."
  type = string
  default = "jdmarble.com"
}
