provider "kubernetes" {
  config_path = "~/.kube/config"
  config_context = "root@k8s.jdmarble.pw"
}

provider "kubectl" {
  config_path = "~/.kube/config"
  config_context = "root@k8s.jdmarble.pw"
}

module "kapp-controller" {
  source = "../modules/kapp-controller"
}
