# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gavinbunney/kubectl" {
  version     = "1.11.1"
  constraints = "1.11.1, ~> 1.11.1"
  hashes = [
    "h1:wTjSEuECh3+Jb1MaPQsmNRbgkcIQ0ej8L4WidWlG/4I=",
    "zh:042ed9e5b3fa118ab7fdf2b7a13a342da0cd6027345d227f0ff14fae77496f67",
    "zh:0997e87ac14e2d986fc80c7f38087381965f6689cc88d118fc98fb3c54dbffa5",
    "zh:3495f6584cb2dfdff7dba33391f8bf6a9bd8bbfe631bb013f9976e7df700c4aa",
    "zh:753ee396444970b702a6b52700ece6e62e6b3e0c2fb06b66785977583c8dc771",
    "zh:9b21d0a86c3a01d094687f37d8ec4156b7568357bf102fb4064a6e3c643cb369",
    "zh:a34f95564109ff87e0833389ac83ab1883f71e21dc2e9614c2c4b4923f27f2c9",
    "zh:abcd981a24d5652a00fb4f7f8b6f3ae023c352446faf2e41578441cab05dba36",
    "zh:caf9ba9bc5606f32aaae8dd3639fcefd8816f440e05c706314cc0001e28eea10",
    "zh:ea8a6e7ce9914d4578f1b44999e1ec343f11087be4da8586b5aec86b6c28152b",
  ]
}

provider "registry.terraform.io/hashicorp/http" {
  version = "2.1.0"
  hashes = [
    "h1:HmUcHqc59VeHReHD2SEhnLVQPUKHKTipJ8Jxq67GiDU=",
    "zh:03d82dc0887d755b8406697b1d27506bc9f86f93b3e9b4d26e0679d96b802826",
    "zh:0704d02926393ddc0cfad0b87c3d51eafeeae5f9e27cc71e193c141079244a22",
    "zh:095ea350ea94973e043dad2394f10bca4a4bf41be775ba59d19961d39141d150",
    "zh:0b71ac44e87d6964ace82979fc3cbb09eb876ed8f954449481bcaa969ba29cb7",
    "zh:0e255a170db598bd1142c396cefc59712ad6d4e1b0e08a840356a371e7b73bc4",
    "zh:67c8091cfad226218c472c04881edf236db8f2dc149dc5ada878a1cd3c1de171",
    "zh:75df05e25d14b5101d4bc6624ac4a01bb17af0263c9e8a740e739f8938b86ee3",
    "zh:b4e36b2c4f33fdc44bf55fa1c9bb6864b5b77822f444bd56f0be7e9476674d0e",
    "zh:b9b36b01d2ec4771838743517bc5f24ea27976634987c6d5529ac4223e44365d",
    "zh:ca264a916e42e221fddb98d640148b12e42116046454b39ede99a77fc52f59f4",
    "zh:fe373b2fb2cc94777a91ecd7ac5372e699748c455f44f6ea27e494de9e5e6f92",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.3.2"
  constraints = "2.3.2"
  hashes = [
    "h1:X+wKrZNYF+qtoBNcMD3cL0+bXSWUDZ7RpHm+2vAwvhE=",
    "zh:10f71c170be13538374a4b9553fcb3d98a6036bcd1ca5901877773116c3f828e",
    "zh:11d2230e531b7480317e988207a73cb67b332f225b0892304983b19b6014ebe0",
    "zh:3317387a9a6cc27fd7536b8f3cad4b8a9285e9461f125c5a15d192cef3281856",
    "zh:458a9858362900fbe97e00432ae8a5bef212a4dacf97a57ede7534c164730da4",
    "zh:50ea297007d9fe53e5411577f87a4b13f3877ce732089b42f938430e6aadff0d",
    "zh:56705c959e4cbea3b115782d04c62c68ac75128c5c44ee7aa4043df253ffbfe3",
    "zh:7eb3722f7f036e224824470c3e0d941f1f268fcd5fa2f8203e0eee425d0e1484",
    "zh:9f408a6df4d74089e6ce18f9206b06b8107ddb57e2bc9b958a6b7dc352c62980",
    "zh:aadd25ccc3021040808feb2645779962f638766eb583f586806e59f24dde81bb",
    "zh:b101c3456e4309b09aab129b0118561178c92cb4be5d96dec553189c3084dca1",
    "zh:ec08478573b4953764099fbfd670fae81dc24b60e467fb3b023e6fab50b70a9e",
  ]
}
