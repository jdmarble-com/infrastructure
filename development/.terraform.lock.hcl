# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/carlpett/sops" {
  version     = "0.6.3"
  constraints = "~> 0.6.3"
  hashes = [
    "h1:1Dx43rBcTvC7sKDvAsLwM038mph2BBLs9VcF4UAGs7A=",
    "zh:0c9f0cad271cbd4c3fa97961b6276ff86f7db2d5dc39ea7de687b5e5091b5f4b",
    "zh:4180cf700dc8ccc71db5d4a0496c22a54301a617ae53d93bba91cb142694552e",
    "zh:656f35d78120bd50d82767a814f53a0a4f96ff1f5f79e40089f259bb1f09ce9e",
    "zh:98e53430bbf13631b314f9107372262151503c133b0551a370e23a0b451f5005",
    "zh:a9c5cdcf12ea89eee54e55779f0e0c868350fc2b48b79a394500d3b04ea28919",
    "zh:b042e80e60c0745ee3c8c49860ecb6e069f75c4c60aa8d6dd2f188f8c0a4f4ab",
    "zh:fe2194ecf065beb9a384b4893cd9d3d975e39db89b38d2b5af05c43352d83397",
  ]
}

provider "registry.terraform.io/fluxcd/flux" {
  version     = "0.1.9"
  constraints = "0.1.9"
  hashes = [
    "h1:FQHWSNW+zKiBB0+iY4QdrIuHl3YjHpwMDv7GHvFyo/M=",
    "zh:27d9c7745379d9f9f74333984272b809ad6b23556ef972336bdce4abe048fb94",
    "zh:355295a33453c313411efe0a87e26f8e7e56d175737e6b59b99eeb8b47c2e4ea",
    "zh:4219b2bec14c542d0bc600e3ad2ab4845c0e92f5817523370cd8565ea96398f3",
    "zh:4bcb10cf947246f2721fcb12ae0b1c1503ebe1f14df4e6ad22adb4ab3bd67b9e",
    "zh:4cfa4ea3ae650dd17753dfa61c69c530eca7342c28de12f7fd2352b0c019f03a",
    "zh:4e378a6daa294e80844e115869b1a8bb08588873b0db01d2bc6cda3a89c6e941",
    "zh:513d59413b80a049e31cb27de4f1dec0a80373bd2f1a58843ba2b714cc61c7d5",
    "zh:58bed9b0372efec6ab40740f00a409de327c68e3d1a71ef1d8e8c751fe2a797b",
    "zh:5b667871b5ae3dd65396e6904c225a04345b65bd00945a2910c7ce4a1a523fe6",
    "zh:6bb8164a19f5e5cad9239bf98e7ab9de8b102dd22a24358514980fbf21d502d3",
    "zh:98dad6ba8672460b6dd4df3c562ae74a194376df6850c6fb5fe6a512f2c001bd",
    "zh:b9f7df432df66d818822d6fd1694d67b9d5a644ca56545f366f0118b05084081",
    "zh:f0cd089b44a38b54d613faeede6677344b4d56da9812e08d142e900632a5b208",
  ]
}

provider "registry.terraform.io/gavinbunney/kubectl" {
  version     = "1.11.1"
  constraints = ">= 1.11.1, 1.11.1"
  hashes = [
    "h1:wTjSEuECh3+Jb1MaPQsmNRbgkcIQ0ej8L4WidWlG/4I=",
    "zh:042ed9e5b3fa118ab7fdf2b7a13a342da0cd6027345d227f0ff14fae77496f67",
    "zh:0997e87ac14e2d986fc80c7f38087381965f6689cc88d118fc98fb3c54dbffa5",
    "zh:3495f6584cb2dfdff7dba33391f8bf6a9bd8bbfe631bb013f9976e7df700c4aa",
    "zh:753ee396444970b702a6b52700ece6e62e6b3e0c2fb06b66785977583c8dc771",
    "zh:9b21d0a86c3a01d094687f37d8ec4156b7568357bf102fb4064a6e3c643cb369",
    "zh:a34f95564109ff87e0833389ac83ab1883f71e21dc2e9614c2c4b4923f27f2c9",
    "zh:abcd981a24d5652a00fb4f7f8b6f3ae023c352446faf2e41578441cab05dba36",
    "zh:caf9ba9bc5606f32aaae8dd3639fcefd8816f440e05c706314cc0001e28eea10",
    "zh:ea8a6e7ce9914d4578f1b44999e1ec343f11087be4da8586b5aec86b6c28152b",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.3.2"
  constraints = ">= 2.0.2, 2.3.2"
  hashes = [
    "h1:X+wKrZNYF+qtoBNcMD3cL0+bXSWUDZ7RpHm+2vAwvhE=",
    "zh:10f71c170be13538374a4b9553fcb3d98a6036bcd1ca5901877773116c3f828e",
    "zh:11d2230e531b7480317e988207a73cb67b332f225b0892304983b19b6014ebe0",
    "zh:3317387a9a6cc27fd7536b8f3cad4b8a9285e9461f125c5a15d192cef3281856",
    "zh:458a9858362900fbe97e00432ae8a5bef212a4dacf97a57ede7534c164730da4",
    "zh:50ea297007d9fe53e5411577f87a4b13f3877ce732089b42f938430e6aadff0d",
    "zh:56705c959e4cbea3b115782d04c62c68ac75128c5c44ee7aa4043df253ffbfe3",
    "zh:7eb3722f7f036e224824470c3e0d941f1f268fcd5fa2f8203e0eee425d0e1484",
    "zh:9f408a6df4d74089e6ce18f9206b06b8107ddb57e2bc9b958a6b7dc352c62980",
    "zh:aadd25ccc3021040808feb2645779962f638766eb583f586806e59f24dde81bb",
    "zh:b101c3456e4309b09aab129b0118561178c92cb4be5d96dec553189c3084dca1",
    "zh:ec08478573b4953764099fbfd670fae81dc24b60e467fb3b023e6fab50b70a9e",
  ]
}

provider "registry.terraform.io/kyma-incubator/kind" {
  version     = "0.0.9"
  constraints = "0.0.9"
  hashes = [
    "h1:YNzKZMWA0LaA06JQwJBiMDn5dQxLbfn8hbGriMyoejs=",
    "zh:402d460a4351d9733ff31de24c39026fb7cb2242d1500f5cf348d17f0ec4643c",
    "zh:9361aca5e41a0906746605d186424b5404681240f7ed17aeefc5e1afd628ee24",
    "zh:e8467161989c61adee2d7d50a31535b07a17e27315b993c36d4fb1271a15a7a7",
  ]
}
