Spin up a local, development cluster:

```bash
$ terraform init
$ terraform apply -auto-approve
$ gpg-export-secret-key-unprotected --armor "929C0AF8F8BA8F2CA8726C6CC351DFC0A61E37FA" | \
  kubectl create secret generic pgp-secrets --namespace=kapp-controller-packaging-global --from-file=sops.asc=/dev/stdin
```

To tear it down, you can `terraform destroy`.

```bash
$ terraform destroy -auto-approve
```
