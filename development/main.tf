module "cluster" {
  source = "../modules/kubernetes/kind"
}

provider "kubernetes" {
  host = module.cluster.host
  client_certificate = module.cluster.client_certificate
  client_key = module.cluster.client_key
  cluster_ca_certificate = module.cluster.cluster_ca_certificate
}

provider "kubectl" {
  host = module.cluster.host
  client_certificate = module.cluster.client_certificate
  client_key = module.cluster.client_key
  cluster_ca_certificate = module.cluster.cluster_ca_certificate
}

module "kapp-controller" {
  source = "../modules/kapp-controller"
}

module "configuration" {
  source = "../modules/configuration"
  depends_on = [
    module.kapp-controller,
  ]
}